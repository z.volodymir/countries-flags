import React, {useEffect, useState, useTransition} from 'react';
import {useNavigate} from "react-router-dom";
import {countriesAPI} from "../api/api";
import {fetchAllCountries} from "../api/config";
import {infoCardData} from "../components/utils";
import {Container} from "../components/Container";
import List from "../components/List";
import Card from "../components/Card/Card";
import Controls from "../components/Controls/Controls";

const HomePage = ({countries, setCountries}) => {
  const [filteredCountries, setFilteredCountries] = useState();
  const [, startTransition] = useTransition();
  const navigate = useNavigate();

  useEffect(() => {
    if (!countries.length) {
      countriesAPI(fetchAllCountries).then(({data}) => setCountries(data))
    }
  }, [countries, setCountries]);

  useEffect(() => {
    setFilteredCountries(countries);
  }, [countries])

  const filterItems = (array, key, value) => {
    return array.filter(item => item[key].toLowerCase().includes(value.toLowerCase()))
  }

  const onHandleSearch = (search, region) => {
    let dataCountries = [...countries];

    if (region) {
      dataCountries = filterItems(dataCountries, 'region', region)
    }
    if (search) {
      dataCountries = filterItems(dataCountries, 'name', search)
    }

    startTransition(() => {
      setFilteredCountries(dataCountries);
    })
  }

  return (
    <Container>
      <Controls onHandleSearch={onHandleSearch}/>
      <List>
        {filteredCountries && filteredCountries.map(country => (
          <Card
            key={country.name}
            onHandleClick={() => navigate(`country/${country.name}`)}
            {...infoCardData(country)}
          />
        ))}
      </List>
    </Container>
  );
};

export default HomePage;