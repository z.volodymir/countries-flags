import React, {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {IoArrowBack} from "react-icons/io5";
import {countriesAPI} from "../api/api";
import {fetchByCountry} from "../api/config";
import {Container} from "../components/Container";
import {Button} from "../components/Button";
import {DetailsInfo} from "../components/DetailsInfo/DetailsInfo";

const DetailsPage = () => {
  const [county, setCountry] = useState();
  const {name} = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    countriesAPI(fetchByCountry(name)).then(({data}) => setCountry(data[0]))
  }, [name, navigate]);

  return (
    <Container>
      <Button onClick={() => navigate(-1)}>
        <IoArrowBack/> Back
      </Button>
      {
        county && <DetailsInfo {...county}/>
      }
    </Container>
  );
};

export default DetailsPage;