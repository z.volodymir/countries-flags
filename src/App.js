import {useState} from "react";
import {Routes, Route, Navigate} from "react-router-dom";
import Layout from "./components/Layout";
import HomePage from "./pages/HomePage";
import DetailsPage from "./pages/DetailsPage";

function App() {
  const [countries, setCountries] = useState([]);

  return (
    <>
      <Routes>
        <Route path="/" element={<Layout/>}>
          <Route index element={<HomePage countries={countries} setCountries={setCountries}/>}/>
          <Route path="/country/:name" element={<DetailsPage/>}/>
          <Route path="*" element={<Navigate to="/"/>}/>
        </Route>
      </Routes>
    </>
  );
}

export default App;
