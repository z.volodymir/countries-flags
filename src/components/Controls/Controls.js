import React, {useEffect, useState} from 'react';
import styled from "styled-components";
import {CustomSelect} from "./CustomSelect";
import Search from "./Search";

const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  row-gap: 2rem;
  padding-bottom: 2rem;

  @media (min-width: 768px) {
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    padding-bottom: 4rem;
  }
`;

const options = [
  { value: 'Africa', label: 'Africa' },
  { value: 'America', label: 'America' },
  { value: 'Asia', label: 'Asia' },
  { value: 'Europe', label: 'Europe' },
  { value: 'Oceania', label: 'Oceania' },
];

const Controls = ({onHandleSearch}) => {
  const [search, setSearch] = useState();
  const [region, setRegion] = useState();

  useEffect(() => {
    const regionValue = region?.value || '';

    onHandleSearch(search, regionValue);
  }, [search, region, onHandleSearch])

  return (
    <Form>
      <Search
        search={search}
        setSearch={setSearch}
      />
      <CustomSelect
        options={options}
        placeholder="Filter by Region"
        isClearable
        value={region}
        onChange={setRegion}
      />
    </Form>
  );
};

export default Controls;