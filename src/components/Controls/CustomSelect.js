import Select from "react-select";
import styled from "styled-components";

export const CustomSelect = styled(Select).attrs({
  styles: {
    control: (provided) => ({
      ...provided,
      height: '50px',
      color: 'var(--colors-text)',
      backgroundColor: 'var(--colors-ui-base)',
      border: 'none',
      borderRadius: 'var(--radii)',
      boxShadow: 'var(--shadow)',
      padding: '.25rem',
    }),
    option: (provided, state) => ({
      ...provided,
      color: 'var(--colors-text)',
      backgroundColor: state.isSelected ? 'var(--colors-bg)' : 'var(--colors-ui-base)',
    }),
  }
})`
  width: 100%;
  max-width: 200px;
  font-family: var(--family);
  border: none;
  border-radius: var(--radii);
  
  & > * {
    box-shadow: var(--shadow);
  }
  
  & * {
    color: var(--colors-text) !important;
  }
  
  & > div[id] {
    background-color: var(--colors-ui-base);
  }
`;