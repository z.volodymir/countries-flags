import styled from "styled-components";
import {IoSearch} from "react-icons/io5";

const Label = styled.label`
  display: flex;
  align-items: center;
  column-gap: 2rem;
  background-color: var(--colors-ui-base);
  border-radius: var(--radii);
  box-shadow: var(--shadow);
  padding: 1rem 2rem;

  @media (min-width: 768px) {
    max-width: 280px;
  }
`;

const Input = styled.input.attrs({
  type: 'search',
  placeholder: 'Search for a country...'
})`
  border: 0;
  outline: 0;
  color: var(--colors-text);
  background-color: var(--colors-ui-base);
`;

const Search = ({search, setSearch}) => {
  return (
    <Label>
      <IoSearch/>
      <Input
        value={search}
        onChange={event => setSearch(event.target.value)}
      />
    </Label>
  );
};

export default Search;