export const infoCardData = (info) => (
  {
    image: info.flags.png,
    title: info.name,
    info: [
      {
        title: 'Population',
        description: info.population.toLocaleString()
      },
      {
        title: 'Region',
        description: info.region
      },
      {
        title: 'Capital',
        description: info.capital
      }
    ]
  }
)

export const infoDetailsData = ({
  nativeName,
  capital,
  population,
  region,
  subregion,
  topLevelDomain = [],
  currencies = [],
  languages = [],
}) => [
  {
    title: 'Native Name',
    description: nativeName
  },
  {
    title: 'Population',
    description: population
  },
  {
    title: 'Region',
    description: region
  },
  {
    title: 'Sub Region',
    description: subregion
  },
  {
    title: 'Capital',
    description: capital
  },
  {
    title: 'Top Level Domain',
    description: <span>{topLevelDomain.join(', ')}</span>
  },
  {
    title: 'Currencies',
    description: currencies.map(({code, name}, idx) => <span key={code}>{getName(currencies, name, idx)} </span>)
  },
  {
    title: 'Languages',
    description: languages.map(({name}, idx) => <span key={name}>{getName(languages, name, idx)} </span>)
  },
]

function getName (array, element, idx) {
  return idx + 1 === array.length ? element : `${element},`;
}