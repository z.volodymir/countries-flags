import React from 'react';
import styled from "styled-components";
import {Outlet} from 'react-router-dom';
import Header from "./Header/Header";

const Main = styled.main`
  padding: 2rem 0;

  @media (min-width: 768px) {
    padding: 4rem 0;
  }
`;

const Layout = () => {
  return (
    <>
      <Header/>
      <Main>
        <Outlet/>
      </Main>
    </>
  );
};

export default Layout;