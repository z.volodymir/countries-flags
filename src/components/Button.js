import styled from "styled-components";

export const Button = styled.button`
  color: var(--colors-text);
  line-height: 2.5;
  
  background-color: var(--colors-ui-base);
  box-shadow: var(--shadow);
  border-radius: var(--radii);
  border: none;
  cursor: pointer;

  display: flex;
  align-items: center;
  gap: .75rem;
  
  padding: 0 1rem;
`;