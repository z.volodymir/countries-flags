import styled from "styled-components";

export const Wrapper = styled.article`
  border-radius: var(--radii);
  box-shadow: var(--shadow);
  background-color: var(--colors-ui-base);
  cursor: pointer;
  overflow: hidden;
`;

export const Image = styled.img.attrs({
  height: '150'
})
`
  width: 100%;
  height: 150px;
  object-fit: cover;
  box-shadow: var(--shadow);
`;

export const Body = styled.div`
  padding: 1rem 1.5rem 2rem;
`;

export const Title = styled.h3`
  font-size: var(--fs-md);
  font-weight: var(--fw-bold);
  margin: 0;
`;

export const List = styled.ul`
  list-style-type: none;
  padding: 1rem 0 0;
  margin: 0;
`;

export const ListItem = styled.li`
  font-size: var(--fs-sm);
  font-weight: var(--fw-light);
  line-height: 1.5;
  
  & > strong {
    font-weight: var(--fw-bold);
  }
`;
