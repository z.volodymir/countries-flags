import React from 'react';
import {List, ListItem} from "./styles";

const CardList = ({listInfo}) => {
  return (
    <List>
      {listInfo.map(({title, description}) => (
        <ListItem key={title}>
          <strong>{title}:</strong> {description}
        </ListItem>
      ))}
    </List>
  );
};

export default CardList;