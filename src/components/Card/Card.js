import {Wrapper, Title, Body, Image} from "./styles";
import CardList from "./CardList";

const Card = ({image, title, info = [], onHandleClick}) => {
  return (
    <Wrapper onClick={onHandleClick}>
      <Image src={image} alt={title}/>
      <Body>
        <Title>{title}</Title>
        <CardList listInfo={info}/>
      </Body>
    </Wrapper>
  );
};

export default Card;