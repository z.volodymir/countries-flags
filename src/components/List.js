import styled from "styled-components";

const Wrapper = styled.section`
  display: grid;
  grid-template-columns: 1fr;
  gap: 2rem;
  
  padding: 2rem 0;

  @media (min-width: 576px) {
    grid-template-columns: repeat(2, 1fr);
  }
  
  @media (min-width: 768px) {
    grid-template-columns: repeat(3, 1fr);
    gap: 3rem;
  }
  
  @media (min-width: 1024px) {
    grid-template-columns: repeat(4, 1fr);
    gap: 4rem;

    padding: 2.5rem 0;
  }
`;

const List = ({children}) => {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  );
};

export default List;