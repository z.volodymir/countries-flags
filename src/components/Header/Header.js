import {useEffect, useState} from "react";
import { IoMoon, IoMoonOutline } from "react-icons/io5";
import {Container} from "../Container";
import {Wrapper, Content, Title, ThemeSwitcher} from "./styles";


const Header = () => {
  const [theme, setTheme] = useState('light');

  useEffect(() => {
    document.body.setAttribute('data-theme', theme);
  }, [theme]);

  const toggleTheme = () => setTheme(theme === 'light' ? 'dark' : 'light')

  return (
    <Wrapper>
      <Container>
        <Content>
          <Title>
            Where in the world?
          </Title>
          <ThemeSwitcher onClick={toggleTheme}>
            {
              theme === 'light'
                ? <IoMoonOutline/>
                : <IoMoon/>
            }
            {theme} Mode
          </ThemeSwitcher>
        </Content>
      </Container>
    </Wrapper>
  );
};

export default Header;