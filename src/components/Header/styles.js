import styled from "styled-components";
import {Link} from "react-router-dom";

export const Wrapper = styled.header`
  box-shadow: var(--shadow);
  background-color: var(--colors-ui-base);
  position: sticky;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1;
`;

export const Content = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 2rem 0;
`;

export const Title = styled(Link).attrs({
  to: '/'
})`
  font-size: var(--fs-sd);
  font-weight: var(--fw-bold);
  color: var(--colors-text);
  text-decoration: none;
`;

export const ThemeSwitcher = styled.div`
  display: flex;
  align-items: center;
  column-gap: .75rem;
  font-size: var(--fs-sd);
  color: var(--colors-text);
  text-transform: capitalize;
  cursor: pointer;
`;