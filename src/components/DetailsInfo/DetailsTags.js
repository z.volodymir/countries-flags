import React, {useEffect, useState} from 'react';
import {useNavigate} from "react-router-dom";
import {countriesAPI} from "../../api/api";
import {fetchByCodes} from "../../api/config";
import {Tag, TagsGroup} from "./styles";

const DetailsTags = ({borders}) => {
  const [borderCountries, setBorderCountries] = useState([]);

  useEffect(() => {
    borders.length && countriesAPI(fetchByCodes(borders)).then(({data}) => setBorderCountries(data));
  }, [borders]);

  const navigate = useNavigate();

  return (
    <TagsGroup>
      {
        !borders.length
          ? <span>There is no border countries</span>
          : borderCountries.map(({name}) => (
            <Tag key={name} onClick={() => navigate(`/country/${name}`)}>{name}</Tag>
          ))
      }
    </TagsGroup>
  );
};

export default DetailsTags;