import styled from "styled-components";

export const Wrapper = styled.section`
  display: grid;
  grid-template-columns: 1fr;
  gap: 2rem;
  margin-top: 3rem;

  @media (min-width: 768px) {
    grid-template-columns: 1fr 1fr;
    align-items: center;
    gap: 5rem;
  }
`;

export const Image = styled.img`
  display: block;
  width: 100%;
  height: auto;
  object-fit: contain;
`;

export const Title = styled.h1`
  font-weight: var(--fw-bold);
  margin: 0;
`;

export const ListGroup = styled.div`
  display: grid;
  row-gap: 2rem;
`;

export const List = styled.ul`
  list-style-type: none;
  display: grid;
  grid-template-columns: 1fr;
  row-gap: 1rem;
  padding: 0;
  margin: 0;
  
  & li:nth-child(6) {
    margin-top: 2rem;
  }

  @media (min-width: 768px) {
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    column-gap: 2rem;
    row-gap: 0;

    & li:nth-child(6) {
      margin-top: 0;
    }

    @media (min-width: 1024px) {
      column-gap: 4rem;
    }
  }
`;

export const ListItem = styled.li`
  flex: 0 1 20%;
  max-width: 50%;
  display: flex;
  flex-wrap: wrap;
  column-gap: .5rem;
  
  & strong {
    font-weight: var(--fw-bold);
  }
`;

export const Meta = styled.div`
  display: flex;
  flex-direction: column;
  column-gap: 1rem;
  row-gap: 2rem;

  & strong {
    font-weight: var(--fw-bold);
    white-space: nowrap;
  }

  @media (min-width: 768px) {
    flex-direction: row;
  }
`;

export const TagsGroup = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 1rem;
`;

export const Tag = styled.span`
  line-height: 1.5;
  background-color: var(--colors-ui-base);
  box-shadow: var(--shadow);
  border-radius: var(--radii);
  cursor: pointer;
  padding: 0 1rem;
`;