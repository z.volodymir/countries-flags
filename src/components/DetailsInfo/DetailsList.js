import React from 'react';
import {List, ListItem} from "./styles";
import {infoDetailsData} from "../utils";

const DetailsList = ({list}) => {
  return (
    <List>
      {
        infoDetailsData(list).map(({title, description}) => (
          <ListItem key={title}>
            <strong>{title}:</strong> {description}
          </ListItem>
        ))
      }
    </List>
  );
};

export default DetailsList;