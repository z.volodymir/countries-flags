import DetailsList from "./DetailsList";
import DetailsTags from "./DetailsTags";
import {Wrapper, Title, Meta, ListGroup, Image} from "./styles";

export const DetailsInfo = (props) => {
  const {
    name,
    flag,
    borders = [],
    ...otherProps
  } = props;

  return (
    <Wrapper>
      <Image src={flag} alt={name}/>
      <ListGroup>
        <Title>{name}</Title>
        <DetailsList list={otherProps}/>
        <Meta>
          <strong>Border Countries: </strong>
          <DetailsTags borders={borders}/>
        </Meta>
      </ListGroup>
    </Wrapper>
  )
}