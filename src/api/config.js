const BASE_URL = 'https://restcountries.com/v2/';

export const fetchAllCountries = `${BASE_URL}all?fields=name,capital,flags,population,region`;

export const fetchByCountry = country => `${BASE_URL}name/${country}`;

export const fetchByCodes = codes => `${BASE_URL}alpha?codes=${codes.join(',')}`;

